package sbu.cs.real;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.io.*;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class App {

    private List<String> urlsToFollow = new ArrayList<>();

    public void start() throws IOException {
        // write something to file
//        String fileName = "test.txt";
//        writeToFile(fileName, "salam");
//        writeToFile2(fileName, "bye");

        //
        urlsToFollow.add("https://en.wikipedia.org/wiki/Mathematics");
        get_and_write_title();
    }

    /**
     * write input to file (not append)
     *
     * @param fileName
     * @param input
     */
    public void writeToFile(String fileName, String input) throws IOException {
        Files.write(Paths.get(fileName), input.getBytes(StandardCharsets.UTF_8));
    }

    public void writeToFile2(String fileName, String input) {
        try (FileWriter fw = new FileWriter(fileName, true)) {
            fw.write(input + "\n");
        } catch (IOException ex) {
            ex.printStackTrace();
        }
    }

    public void get_and_write_title() throws IOException {
        System.out.println("salam");
        if (urlsToFollow.size() == 0) {
            return;
        }
        if (urlsToFollow.size() > 10) {
            Collections.shuffle(urlsToFollow);
            urlsToFollow = urlsToFollow.subList(0, 10);
        }
        List<Document> documents = new ArrayList<>();
        for (String url : urlsToFollow) {
            documents.add(Jsoup.connect(url).get());
        }
        urlsToFollow.clear();
        for (Document doc : documents) {
            extract_title(doc);
            follow_links(doc);
        }
        System.out.println(urlsToFollow.size());
        get_and_write_title();
    }

    private void follow_links(Document doc) throws IOException {
        Elements elements = doc.getElementsByTag("a");
        for (Element element : elements) {
            String href = element.attr("href");
            if (href.startsWith("/wiki/")) {
                String url = createUrlFromTitle(href);
                urlsToFollow.add(url);
            }
        }
    }

    private String createUrlFromTitle(String href) {
        return "http://www.wikipedia.com" + href;
    }

    private void extract_title(Document doc) {
        Element firstHeading = doc.getElementById("firstHeading");
        writeToFile2("titles.txt", firstHeading.text());
        System.out.println("new title added: " + firstHeading.text());
    }
}
