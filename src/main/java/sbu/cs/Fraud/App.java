package sbu.cs.Fraud;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;

public class App {

    public void start() throws IOException {
        // write to file test
//        String file_name = "test.txt";
//        appendToFile(file_name, "salam");
//        appendToFile(file_name, "ali");

        // jsoup
        find_wikipedia_title("https://en.wikipedia.org/wiki/Math");
    }

    public void appendToFile(String fileName, String input) throws IOException {
        FileWriter fw = new FileWriter(fileName, true);
//        BufferedWriter bw = new BufferedWriter(fw);
//        PrintWriter out = new PrintWriter(bw);
        fw.write(input + "\n");
        fw.flush();
        fw.close();
    }

    public void find_wikipedia_title(String page) {
        page = fix_url(page);
        if (page == null) {
            return;
        }
        Document doc = null;
        try {
            System.out.println(page);
            doc = Jsoup.connect(page).get();
            appendToFile("titles.txt", extractTitle(doc));
        } catch (IOException e) {
            e.printStackTrace();
        }
        Elements links = doc.getElementsByTag("a");
        for (Element link : links) {
            String href = link.attr("href");
            find_wikipedia_title(href);
        }
    }

    private String fix_url(String page) {
        if (page.trim().isEmpty()) {
            return null;
        } else if (page.startsWith("http")) {
            return page;
        } else if (page.startsWith("#")) {
            return null;
        } else {
            return "http://www.wikipedia.com" + page;
        }
    }

    public String extractTitle(Document doc) {
        String tmp = doc.getElementById("firstHeading").text();
        System.out.println(tmp);
        return tmp;
    }
}
